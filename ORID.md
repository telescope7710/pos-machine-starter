## O

- I learnt how to splite hard task into small ones.
- I learnt how to draw context map to visualize results of Tasking.
- I learnt how to use git and IDE more efficiently.
- We draw context map by ourselves to practice.

## R
I gained a lot.

## I
It is important to thoroughly understand the requirements when tasking. And having good habits when using git is key to team development. It's worth spending time on Tasking and make Context Map making development smoother.

## D
I will try to train myself in Tasking.