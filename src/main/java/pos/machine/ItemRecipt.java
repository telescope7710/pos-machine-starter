package pos.machine;

public class ItemRecipt {
    private final String name;
    private final Integer quantity;

    private final int unitPrice;
    private final int subTotal;

    public ItemRecipt(String name, Integer quantity, int unitPrice, int subTotal) {
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.subTotal = subTotal;
    }

    public String getName() {
        return name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public int getSubTotal() {
        return subTotal;
    }
}
