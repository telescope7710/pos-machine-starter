package pos.machine;

import java.util.*;

public class PosMachine {
    List<Item> items = ItemsLoader.loadAllItems();
    public String printReceipt(List<String> barcodes) {
        HashMap itemCountMap = getItemCountMap(barcodes);
        List<ItemRecipt> itemReciptList = createItemReceipt(itemCountMap);

        String receipt = generateReceipt(itemReciptList);
        return receipt;
    }

    String generateReceipt(List<ItemRecipt> itemReciptList) {
        String ItemReceiptLines = formatItemReceipt(itemReciptList);
        String totalLine = formatTotalLine(itemReciptList);
        String receipt = "***<store earning no money>Receipt***"+"\n"+ItemReceiptLines+ "----------------------\n"+totalLine+"\n"+"**********************";
        return receipt;
    }

    String formatTotalLine(List<ItemRecipt> itemReciptList) {
        int total = calculateTotal(itemReciptList);
        return new String("Total: "+total+" (yuan)");
    }

    String formatItemReceipt(List<ItemRecipt> itemReciptList) {
        StringBuilder lines = new StringBuilder("");
        for (int i = 0; i < itemReciptList.size(); i++) {
            String line = "Name: "+itemReciptList.get(i).getName()+", Quantity: "+itemReciptList.get(i).getQuantity()+", Unit price: " + itemReciptList.get(i).getUnitPrice()+" (yuan), Subtotal: "+ itemReciptList.get(i).getSubTotal()+" (yuan)\n";
            lines = lines.append(line);
        }
        return new String(lines);
    }

    int calculateTotal(List<ItemRecipt> itemReciptList){
        int total = 0;
        for (int i = 0; i < itemReciptList.size(); i++) {
            total += itemReciptList.get(i).getSubTotal();
        }
        return total;
    }
     List<ItemRecipt> createItemReceipt(HashMap itemCountMap) {
        List<ItemRecipt> itemRecipts = new ArrayList<>();

        Iterator<String> iterator = itemCountMap.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            ItemRecipt itemRecipt = createItemLine(key,(Integer)itemCountMap.get(key));
            itemRecipts.add(itemRecipt);
        }
        return itemRecipts;
    }

    ItemRecipt createItemLine(String key, Integer value) {
        Item item = getItemByBarcode(key);
        String name = item.getName();
        Integer quantity = value;
        int unitPrice = item.getPrice();
        int subTotal = item.getPrice() * quantity;
        return new ItemRecipt(name,quantity,unitPrice,subTotal);
    }

    HashMap getItemCountMap(List<String> itemBarcodes) {
        HashMap<String, Integer> itemCountMap = new HashMap<>();
        for (int i = 0; i < itemBarcodes.size(); i++) {
            String barcode = itemBarcodes.get(i);
            if(getItemByBarcode(barcode) != null){
                if(itemCountMap.containsKey(barcode)){
                    itemCountMap.compute(barcode,(key,value)->value + 1);
                }else {
                    itemCountMap.put(barcode,1);
                }
            }
        }
        return itemCountMap;
    }
    Item getItemByBarcode(String barcode){
        for (int i = 0; i < items.size(); i++) {
            if(items.get(i).getBarcode().equals(barcode)){
                return items.get(i);
            }
        }
        return null;
    }
}
